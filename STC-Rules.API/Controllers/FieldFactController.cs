﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using STC_Rules.API.Facts;
using STC_Rules.API.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

namespace STC_Rules.API.Controllers
{
    [Route("fields")]
    [ApiController]
    public class FieldFactController : Controller
    {

        private readonly ILogger<RulesController> _logger;
        private readonly IRepository<FieldFact> _fieldsRepo;


        public FieldFactController(ILogger<RulesController> logger, IRepository<FieldFact> fieldsRepo)
        {
            _logger = logger;
            _fieldsRepo = fieldsRepo;
        }

        [HttpGet]
        public IActionResult GetFields([FromQuery] Dictionary<string, string> query)
        {
            if (query.TryGetValue("parentId", out var pid))
            {
                var parentId = new Guid(pid);
                var ret = _fieldsRepo.AsQueryable();
                if (parentId != default)
                {
                    ret = ret.Where(p => p.ParentId == parentId);
                }
                else
                {
                    return BadRequest("ParentId is required");
                }
                return Ok(ret);
            }
            return BadRequest("ParentId is required");
        }

        [HttpPost]
        public FieldFact[] CreateFields([FromBody] FieldFact[] model, [FromQuery] Dictionary<string, string> query)
        {
            if (query.TryGetValue("parentId", out var pid))
            {
                var parentId = new Guid(query["parentId"]);
                foreach (var item in model)
                {
                    item.ParentId = parentId;
                    _fieldsRepo.Save(item);
                }
            }
            return model;
        }

        [HttpPut]
        public FieldFact[] UpdateFields([FromBody] FieldFact[] model)
        {
            foreach (var item in model)
            {
                _fieldsRepo.Update(item);
            }
            return model;
        }

        [HttpPost]
        [Route("form/body")]
        public IActionResult GenerateFieldsFromBody([FromForm] IFormCollection col, [FromQuery] Dictionary<string, string> query)
        {
            if (query.TryGetValue("parentId", out var pid))
            {
                var parentId = new Guid(pid);
                var dynamicFields = col.ToDictionary(p => p.Key, v => v.Value.ToString());
                var fieldValues = dynamicFields.Select(p => new FieldFact { Identifier = p.Key, ParentId = parentId }).ToList();
                return Ok(fieldValues);
            }
            return BadRequest();
        }

        [HttpPost]
        [Route("json/body")]
        public IActionResult GenerateFieldsFromBody([FromBody] IDictionary<string, object> model, [FromQuery] Dictionary<string, string> query)
        {
            if (query.TryGetValue("parentId", out var parentId))
            {
                var dynamicFields = model.ToDictionary(k => k.Key, p => (JsonElement)p.Value).ToDictionary();
                var fieldValues = dynamicFields.Select(p => new FieldFact { Identifier = p.Key, ParentId = new Guid(parentId) }).ToList();
                return Ok(fieldValues);
            }
            return BadRequest();

        }
    }
}
