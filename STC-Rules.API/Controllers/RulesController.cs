﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using STC_Rules.API.Facts;
using STC_Rules.API.Storage;
using System;
using System.Collections.Generic;
using System.Linq;

namespace STC_Rules.API.Controllers
{
    [Route("rules")]
    public class RulesController : ControllerBase
    {

        private readonly ILogger<RulesController> _logger;
        private readonly IRepository<RuleFact> _ruleRepo;


        public RulesController(ILogger<RulesController> logger, IRepository<RuleFact> ruleRepo)
        {
            _logger = logger;
            _ruleRepo = ruleRepo;
        }

        [HttpGet]
        public IActionResult GetRules([FromQuery] IDictionary<string, string> query)
        {
            IEnumerable<RuleFact> rules = null;
            if (query.TryGetValue("parentId", out var pid)) 
            {
                var parentId = new Guid(pid);
                if (parentId == default)
                {
                    return BadRequest();
                }
                rules = _ruleRepo.AsQueryable().Where(p => p.ParentId == parentId);
            }
            return Ok(rules);
        }

        [HttpPost]
        public RuleFact[] CreateRules([FromBody] RuleFact[] rules, [FromQuery] IDictionary<string, string> query)
        {
            //TODO get ruleset and make sure it exists
            var parentId = new Guid(query["parentId"]);
            foreach (var model in rules)
            {
                model.ParentId = parentId;
                _ruleRepo.Save(model);
            }
            
            return rules;
        }

        [HttpPut]
        public RuleFact UpdateRule([FromBody] RuleFact model)
        {
            var rule = _ruleRepo.Save(model);
            return rule;
        }
    }
}
