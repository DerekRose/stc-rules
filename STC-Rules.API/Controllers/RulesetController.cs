﻿using Microsoft.AspNetCore.Mvc;
using STC_Rules.API.Models;
using STC_Rules.API.Storage;
using System;
using System.Collections.Generic;
using System.Linq;

namespace STC_Rules.API.Controllers
{
    [Route("rulesets")]
    public class RulesetController : Controller
    {
        private readonly IRepository<Ruleset> _rulesetRepo;

        public RulesetController(IRepository<Ruleset> rulesetRepo)
        {
            _rulesetRepo = rulesetRepo;
        }


        [HttpGet]
        public IActionResult GetRulesets([FromQuery] IDictionary<string, string> query)
        {
            IEnumerable<Ruleset> ruleset = null;
            if (query.TryGetValue("id", out var id))
            {
                var resourceId = new Guid(id);
                if (resourceId == default)
                {
                    return BadRequest();
                }
                ruleset = _rulesetRepo.AsQueryable().Where(p => p.Id == resourceId);
            }
            else if (query.TryGetValue("parentId", out var pid))
            {
                var parentId = new Guid(pid);
                if (parentId == default)
                {
                    return BadRequest();
                }
                ruleset = _rulesetRepo.AsQueryable().Where(p => p.ParentId == parentId);
            }
            return Ok(ruleset);
        }

        [HttpPost]
        public Ruleset CreateRuleset([FromBody] Ruleset model)
        {
            var ruleset = _rulesetRepo.Save(model);
            return ruleset;
        }

        [HttpPut]
        public Ruleset UpdateRuleset([FromBody] Ruleset model)
        {
            var ruleset = _rulesetRepo.Update(model);
            return ruleset;
        }
    }
}
