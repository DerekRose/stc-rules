﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using STC_Rules.API.Facts;
using STC_Rules.API.Models;
using STC_Rules.API.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

namespace STC_Rules.API.Controllers
{
    [Route("sessions")]
    [ApiController]
    public class SessionController : Controller
    {
        private readonly IRepository<Ruleset> _rulesetRepo;
        private readonly IRepository<RuleFact> _rulesRepo;
        private readonly IRepository<FieldFact> _fieldsRepo;
        private readonly NRules.ISession _session;

        public SessionController(IRepository<Ruleset> rulesetRepo, IRepository<RuleFact> rulesRepo, IRepository<FieldFact> fieldsRepo, NRules.ISession session)
        {
            _rulesetRepo = rulesetRepo;
            _rulesRepo = rulesRepo;
            _fieldsRepo = fieldsRepo;
            _session = session;
        }
        
        [HttpPost]
        [Route("forms")]
        public IActionResult RunRulesetForm([FromForm] IFormCollection col, [FromQuery] Dictionary<string, string> query)
        {
            var t = this.ControllerContext;
            var dynamicFields = col.ToDictionary(p => p.Key, v => v.Value.ToString());


            var ruleset = _rulesetRepo.AsQueryable().FirstOrDefault(p => p.Id == new Guid(query["parentId"]));
            var rules = _rulesRepo.AsQueryable().Where(p => p.ParentId == ruleset.Id).ToList();
            var fields = _fieldsRepo.AsQueryable().Where(p => p.ParentId == ruleset.Id).ToList();
            foreach (var field in fields)
            {
                field.Value = (dynamicFields?.FirstOrDefault(p => p.Key == field.Identifier))?.Value ?? string.Empty;
            }

            foreach (var field in fields)
            {
                _session.Insert(field);
            }
            foreach (var rule in rules)
            {
                _session.Insert(rule);
            }
            _session.Insert(ruleset);

            var result = _session.Fire();

            return Ok(new SessionTracker
            {
                Fields = _session.Query<FieldFact>().ToArray(),
                ObservedRules = _session.Query<RuleObservedFact>().ToArray(),
                Rules = _session.Query<RuleFact>().ToArray()
            });
        }


        [HttpPost]
        [Route("json")]
        public IActionResult RunRulesetJson([FromBody] Dictionary<string, object> data, [FromQuery] Dictionary<string, string> query)
        {
            var dynamicFields = data.ToDictionary(k => k.Key, p => (JsonElement)p.Value).ToDictionary();
            var ruleset = _rulesetRepo.AsQueryable().FirstOrDefault(p => p.Id == new Guid(query["rulesetId"]));
            var rules = _rulesRepo.AsQueryable().Where(p => p.ParentId == ruleset.Id).ToList();
            var fields = _fieldsRepo.AsQueryable().Where(p => p.ParentId == ruleset.Id).ToList();
            foreach (var field in fields)
            {
                field.Value = (dynamicFields?.FirstOrDefault(p => p.Key == field.Identifier))?.Value ?? string.Empty;
            }

            foreach (var field in fields)
            {
                _session.Insert(field);
            }
            foreach (var rule in rules)
            {
                _session.Insert(rule);
            }
            _session.Insert(ruleset);

            var result = _session.Fire();

            return Ok(new SessionTracker
            {
                Fields = _session.Query<FieldFact>().ToArray(),
                ObservedRules = _session.Query<RuleObservedFact>().ToArray(),
                Rules = _session.Query<RuleFact>().ToArray()
            });
        }
    }
}
