﻿using NRules.Extensibility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STC_Rules.API
{
    public class DependencyResolver : IDependencyResolver
    {
        private IServiceProvider _container;
        public DependencyResolver(IServiceProvider container)
        {
            _container = container;
        }

        public object Resolve(IResolutionContext context, Type serviceType)
        {
            return _container.GetService(serviceType);
        }

        public object Resolve<T>()
        {
            return _container.GetService(typeof(T));
        }
    }
}
