﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace STC_Rules.API.Facts
{
    public class FieldFact
    {
        public Guid Id { get; set; }
        public string Identifier { get; set; }
        public List<FieldValue> Values { get; private set; }
        public Guid ParentId { get; set; }
        public string FieldType { get; set; }
        public int Index { get; set; }
        public Dictionary<string,string> Attributes { get; set; }

        public string Value
        {
            get
            {
                return Values?.OrderByDescending(p => p.Timestamp)?.FirstOrDefault()?.Value;
            }
            set
            {
                if (value != null) Values.Add(new FieldValue(value));
            }
        }
        public List<ResultFact> Results { get; set; }
        public ResultFact Result
        {
            get
            {
                return Results?.OrderByDescending(p => p.Timestamp)?.FirstOrDefault();
            }
            set
            {
                if (value != null) Results.Add(value);
            }
        }

        public FieldFact()
        {
            Results = new List<ResultFact>();
            Values = new List<FieldValue>();
        }
    }

    public class FieldValue
    {
        public string Value { get; set; }
        public DateTime Timestamp { get; set; }

        public FieldValue(string value)
        {
            Value = value;
            Timestamp = DateTime.UtcNow;
        }
    }
}
