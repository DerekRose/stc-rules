﻿using STC_Rules.API.Models;

namespace STC_Rules.API.Facts
{
    public class HL7Fact
    {
        public string RawMessage { get; private set; }
        public RuleContextFact Context { get; set; }
        public DTO Model { get; set; }

        public HL7Fact()
        {

        }
    }
}
