﻿using System;
namespace STC_Rules.API.Facts
{
    public class ResultFact
    {
        public bool Valid { get; set; }
        public string Message { get; set; }
        public DateTime Timestamp { get; set; }
        public RuleType RuleType { get; set; }

        public ResultFact(bool valid, RuleType ruleType, string message = null)
        {
            Valid = valid;
            Timestamp = DateTime.UtcNow;
            Message = message;
            RuleType = ruleType;
        }
    }
}
