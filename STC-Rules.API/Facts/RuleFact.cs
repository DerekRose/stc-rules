﻿using System;
using System.Collections.Generic;

namespace STC_Rules.API.Facts
{
    public class RuleFact
    {
        public Guid Id { get; set; }
        public Guid ParentId { get; set; }
        public List<string> Identifiers { get; set; }
        public RuleType RuleType { get; set; }
        public Dictionary<string, string> Attributes { get; set; }
        public int ProcessingStage { get; set; }

        public RuleFact()
        {

        }
    }

    public enum RuleType
    {
        UNDEFINED,
        REQUIRED,
        VALUES_MAPPING,
        ALLOWED_VALUES,
        POCO_MAPPING
    }
}