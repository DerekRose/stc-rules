﻿using System;

namespace STC_Rules.API.Facts
{
    public class RuleObservedFact
    {
        public Guid CorrelationId { get; set; }
        public ResultFact Result { get; set; }
        public int ProcessingStage { get; set; }

        public RuleObservedFact(Guid correlationId, ResultFact result)
        {
            CorrelationId = correlationId;
            Result = result;
        }
    }
}
