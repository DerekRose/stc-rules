﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace STC_Rules.API.Models
{
    public class DTO
    {
        public DTO()
        {

        }

        public string AbnormalFlag { get; set; }
        public string Comments { get; set; }
        public string DateResultReleased { get; set; }
        public string[] DiseaseSymptoms { get; set; }
        public string EmployedInHealthcare { get; set; }
        public string EmployedInHighRiskSetting { get; set; }
        public string FileCreatedDate { get; set; }
        public string FirstTest { get; set; }
        public string FlatfileVersionNo { get; set; }
        public string Hospitalized { get; set; }
        public string Icu { get; set; }
        public string IllnessOnsetDate { get; set; }
        public string InstrumentInstanceId { get; set; }
        public string InstrumentModelId { get; set; }
        public string InstrumentModelName { get; set; }
        public string LinkTestToParentAccession { get; set; }
        public string LinkTestToParentResult { get; set; }
        public string OrderResultStatus { get; set; }
        public string OrderTestDate { get; set; }
        public string OrderedTestCode { get; set; }
        public string OrderedTestCodeSystem { get; set; }
        public string OrderedTestDescription { get; set; }
        public string OrderingFacilityCity { get; set; }
        public string OrderingFacilityCounty { get; set; }
        public string OrderingFacilityName { get; set; }
        public string OrderingFacilityPhoneNumber { get; set; }
        public string OrderingFacilityState { get; set; }
        public string OrderingFacilityStreet { get; set; }
        public string OrderingFacilityStreet2 { get; set; }
        public string OrderingFacilityZipCode { get; set; }
        public string OrderingProviderCity { get; set; }
        public string OrderingProviderCounty { get; set; }
        public string OrderingProviderFirstName { get; set; }
        public string OrderingProviderId { get; set; }
        public string OrderingProviderLastName { get; set; }
        public string OrderingProviderPhone { get; set; }
        public string OrderingProviderState { get; set; }
        public string OrderingProviderStreet { get; set; }
        public string OrderingProviderStreet2 { get; set; }
        public string OrderingProviderZipCode { get; set; }
        public string PatientAge { get; set; }
        public string PatientAgeUnits { get; set; }
        public string PatientCity { get; set; }
        public string PatientCounty { get; set; }
        public string PatientDeathDate { get; set; }
        public string PatientDied { get; set; }
        public string PatientDob { get; set; }
        public string PatientEthnicity { get; set; }
        public string PatientFirstName { get; set; }
        public string PatientGender { get; set; }
        public string PatientId { get; set; }
        public string PatientId2 { get; set; }
        public string PatientId2Assigner { get; set; }
        public string PatientId2Type { get; set; }
        public string PatientIdAssigner { get; set; }
        public string PatientIdType { get; set; }
        public string PatientLastName { get; set; }
        public string PatientLocation { get; set; }
        public string PatientMiddleName { get; set; }
        public string PatientOccupation { get; set; }
        public string PatientPhoneNumber { get; set; }
        public string PatientRace { get; set; }
        public string PatientResidencyType { get; set; }
        public string PatientState { get; set; }
        public string PatientStreet { get; set; }
        public string PatientStreet2 { get; set; }
        public string PatientSuffix { get; set; }
        public string PatientZipCode { get; set; }
        public string Pregnant { get; set; }
        public string PublicHealthCaseId { get; set; }
        public string ReferenceRange { get; set; }
        public string ReportFacilDataSourceApp { get; set; }
        public string ReportingFacilityId { get; set; }
        public string ReportingFacilityName { get; set; }
        public string ResidentCongregateSetting { get; set; }
        public string ResultFormat { get; set; }
        public string SpecimenCollectionDateTime { get; set; }
        public string SpecimenReceivedDateTime { get; set; }
        public string SpecimenSourceSiteCode { get; set; }
        public string SpecimenSourceSiteCodeSys { get; set; }
        public string SpecimenSourceSiteDescrip { get; set; }
        public string SpecimenTypeCode { get; set; }
        public string SpecimenTypeCodeSystem { get; set; }
        public string SpecimenTypeDescription { get; set; }
        public string SpecimenTypeFreeText { get; set; }
        public string SubmitterSampleIdAssigner { get; set; }
        public string SubmitterUniqueSampleId { get; set; }
        public string SymptomaticForDisease { get; set; }
        public string TestDate { get; set; }
        public string TestKitEuaId { get; set; }
        public string TestKitInstanceId { get; set; }
        public string TestKitModelId { get; set; }
        public string TestKitModelName { get; set; }
        public string TestMethodDescription { get; set; }
        public string TestPerformedCode { get; set; }
        public string TestPerformedCodeSystem { get; set; }
        public string TestPerformedDescription { get; set; }
        public string TestPerformedNumber { get; set; }
        public string TestResultCodeSystem { get; set; }
        public string TestResultCoded { get; set; }
        public string TestResultComparator { get; set; }
        public string TestResultDescription { get; set; }
        public string TestResultFreeText { get; set; }
        public string TestResultNumber { get; set; }
        public string TestResultNumber2 { get; set; }
        public string TestResultNumberSeparator { get; set; }
        public string TestResultStatus { get; set; }
        public string TestResultSubId { get; set; }
        public string TestResultUnits { get; set; }
        public string TestingLabAccessionNumber { get; set; }
        public string TestingLabCity { get; set; }
        public string TestingLabCounty { get; set; }
        public string TestingLabId { get; set; }
        public string TestingLabName { get; set; }
        public string TestingLabSpecimenId { get; set; }
        public string TestingLabState { get; set; }
        public string TestingLabStreet { get; set; }
        public string TestingLabStreet2 { get; set; }
        public string TestingLabZipCode { get; set; }
        public string TravelHistory { get; set; }
        public string LocationId { get; set; }
        public string TenantID { get; set; }
    }
}
