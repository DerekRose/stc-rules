﻿using STC_Rules.API.Facts;
using System;
using System.Collections.Generic;

namespace STC_Rules.API.Models
{
    public class Ruleset
    {
        public string Name { get; set; }
        public Guid Id { get; set; }
        public Guid ParentId { get; set; }
        public Dictionary<string, string> Attributes { get; set; }
        public RuleConfigurationFact RuleConfiguration { get; set; }

        public Ruleset()
        {

        }
    }
}
