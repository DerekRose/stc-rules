﻿using STC_Rules.API.Facts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STC_Rules.API.Models
{
    public class SessionTracker
    {
        public bool SessionPassed
        {
            get
            {
                return ObservedRules == null || ObservedRules.All(p => p.Result.Valid == true);
            }
        }
        public FieldFact[] Fields { get; set; }
        public RuleFact[] Rules { get; set; }
        public RuleObservedFact[] ObservedRules { get; set; }

        public SessionTracker()
        {

        }
    }
}
