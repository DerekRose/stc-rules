using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json.Linq;
using STC_Rules.API.Facts;
using STC_Rules.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace STC_Rules.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Dictionary<string, object> g = default;
            var t = (g?.FirstOrDefault())?.Value;
            // The following statement allows you to call insecure services. To be used only in development environments.
            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
