﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace STC_Rules.API.Providers
{
    public interface IGrpcProvider
    {
        public IDictionary<string, string> MapValues(IEnumerable<string> values);
        public List<string> GetList(Guid listId);
    }

    public class GrpcProvider : IGrpcProvider
    {
        public IDictionary<string, string> MapValues(IEnumerable<string> values)
        {
            return values.ToDictionary(p => p, k => Guid.NewGuid().ToString());
        }

        public List<string> GetList(Guid listId)
        {
            return new List<string> { };
        }
    }
}
