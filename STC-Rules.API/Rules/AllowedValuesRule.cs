﻿using NRules.RuleModel;
using STC_Rules.API.Providers;
using STC_Rules.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using STC_Rules.API.Facts;

namespace STC_Rules.API.Rules
{
    public class AllowedValuesRule : RuleBase
    {
        public AllowedValuesRule() : base(RuleType.ALLOWED_VALUES)
        {

        }

        public override void Define()
        {
            base.Define();
        }

        protected override void Process(RuleFact rule, IEnumerable<FieldFact> fields, IContext ctx, IServiceProvider container)
        {
            ResultFact result = null;
            try
            {
                var grpc = (IGrpcProvider)container.GetService(typeof(IGrpcProvider));
                var listId = rule.Attributes["listId"];
                var caseSensative = rule.Attributes["caseSensative"];
                var comparer = StringComparison.InvariantCulture;
                //default to case sensative
                if (bool.TryParse(caseSensative, out var cs) && !cs)
                {
                    comparer = StringComparison.InvariantCultureIgnoreCase;
                }
                var allowedValues = grpc.GetList(Guid.Parse(listId));
                var checkFields = fields.Where(p => rule.Identifiers.Contains(p.Identifier)).ToList();
                var failedFields = checkFields.Where(p => !allowedValues.Any(x => x.Equals(p.Value, comparer)));
                foreach (var fld in fields.Except(failedFields))
                {
                    fld.Results.Add(new ResultFact(true, RuleType.ALLOWED_VALUES));
                }
                foreach (var fld in failedFields)
                {
                    fld.Results.Add(new ResultFact(false, RuleType.ALLOWED_VALUES));
                }
                result = new ResultFact(true, RuleType.ALLOWED_VALUES);
            }
            catch (Exception ex)
            {
                result = new ResultFact(false, RuleType.ALLOWED_VALUES, ex.Message);
            }
            ctx.Insert(new RuleObservedFact(rule.Id, result));
        }
    }
}
