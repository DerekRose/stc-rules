﻿using NRules.RuleModel;
using STC_Rules.API.Facts;
using STC_Rules.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace STC_Rules.API.Rules
{
    public class POCOMappingRule : RuleBase
    {
        public POCOMappingRule() : base(RuleType.POCO_MAPPING)
        {

        }

        public override void Define()
        {
            base.Define();
        }

        protected override void Process(RuleFact rule, IEnumerable<FieldFact> fields, IContext ctx, IServiceProvider container)
        {
            ResultFact result = null;
            var model = new DTO();
            foreach (var field in rule.Identifiers)
            {
                var split = field.Split(':');
                var from = fields.FirstOrDefault(p => p.Identifier == split.First());
                PropertyInfo prop = typeof(DTO).GetProperty(split.Last(), BindingFlags.Public | BindingFlags.Instance);
                if (null != prop && prop.CanWrite)
                {
                    prop.SetValue(model, from.Value, null);
                }
            }
            foreach (var fld in fields)
            {
                fld.Results.Add(new ResultFact(true, RuleType.POCO_MAPPING));

            }
            result = new ResultFact(true, RuleType.POCO_MAPPING);
            ctx.Insert(new RuleObservedFact(rule.Id, result));
        }
    }
}
