﻿using NRules.Fluent.Dsl;
using NRules.RuleModel;
using System;
using System.Collections.Generic;
using System.Linq;
using STC_Rules.API.Facts;
using STC_Rules.API.Models;
using STC_Rules.API.Providers;

namespace STC_Rules.API.Rules
{
    public class RequiredRule : RuleBase
    {

        public RequiredRule() : base(RuleType.REQUIRED)
        {

        }

        public override void Define()
        {
            base.Define();
        }

        protected override void Process(RuleFact rule, IEnumerable<FieldFact> fields, IContext ctx,
            IServiceProvider container)
        {
            ResultFact result = null;
            try
            {
                foreach (var fld in fields)
                {

                    if (rule.Identifiers.Any(p => string
                        .Equals(p, fld.Identifier, StringComparison.InvariantCultureIgnoreCase)))
                    {
                        fld.Results.Add(new ResultFact(!string.IsNullOrEmpty(fld.Value?.Trim()), RuleType.REQUIRED));

                    }
                    else
                    {
                        fld.Results.Add(new ResultFact(true, RuleType.REQUIRED));
                    }
                }

                foreach (var field in rule.Identifiers.Except(fields.Select(p => p.Identifier)))
                {
                    ctx.Insert(new FieldFact
                    {
                        Identifier = field,
                        //default to invalid if field is not present
                        Results = new List<ResultFact> { new ResultFact(false, RuleType.REQUIRED, $"'{field}' is required") }
                    });
                }
                ctx.UpdateAll(fields);

                result = new ResultFact(fields.All(p => p.Result.Valid), RuleType.REQUIRED);
            }
            catch (Exception ex)
            {
                result = new ResultFact(false, RuleType.REQUIRED, ex.Message);
            }

            ctx.Insert(new RuleObservedFact(rule.Id, result));
        }
    }
}
