﻿using NRules.Fluent.Dsl;
using NRules.RuleModel;

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using STC_Rules.API.Facts;
using STC_Rules.API.Models;

namespace STC_Rules.API.Rules
{
    public abstract class RuleBase : Rule
    {
        protected RuleType _ruleType = default;

        public RuleBase(RuleType ruleType)
        {
            _ruleType = ruleType;
        }

        public override void Define()
        {
            Ruleset _ruleset = default;
            RuleFact _rule = default;
            IEnumerable<RuleFact> _rules = default;
            IEnumerable<FieldFact> _fields = default;
            IServiceProvider _container = default;
            //Dictionary<string, string> _fieldValues = default;
            IEnumerable<RuleObservedFact> _observations = default;

            Dependency().Resolve<IServiceProvider>(() => _container);
            //When()
            //    .Match<Ruleset>(() => _ruleset, c => c.RuleConfiguration.RunBasicRules)
            //    .Match<RuleFact>(() => _rule, c => c.RuleType == _ruleType)
            //    .Or(a => a.Exists<RuleObservedFact>(c => c.ProcessingStage == _rule.ProcessingStage - 1).Exists<RuleFact>(c => c.Id == _rule.Id && c.ProcessingStage == 0))
            //    //make sure rule quits once firing once its completed
            //    .Not<RuleObservedFact>(c => c.CorrelationId == _rule.Id)
            //    .Match<DTO>(() => _dto)
            //    .Query(() => _fields, x => x
            //        .Match<FieldFact>()
            //        .Collect()
            //        .Where(c => c.Any()));

            When()
                .Match<Ruleset>(() => _ruleset, c => c.RuleConfiguration.RunBasicRules)
                .Query(() => _rules, x => x
                    .Match<RuleFact>(c => c.RuleType == _ruleType)
                    .Collect()
                    .Where(c => c.Any()))
                .Query(() => _observations, x => x
                    .Match<RuleObservedFact>()
                    .Collect()
                    .Where(c => Test(_rules, c, _ruleType, out _rule)))
                    //.Match<Dictionary<string, string>>(() => _fieldValues)
                    .Query(() => _fields, x => x
                        .Match<FieldFact>()
                        .Collect()
                        .Where(c => c.Any()));

            //    .Match<RuleFact>(() => _rule, c => c.RuleType == _ruleType)
            //    .Or(a => a
            //        .Exists<RuleObservedFact>(c => c.ProcessingStage == _rule.ProcessingStage - 1)
            //        .Exists<RuleFact>(p => p.Id == _rule.Id && p.ProcessingStage == 0)
            //        )
            //    //make sure rule quits once firing once its completed
            //    .Not<RuleObservedFact>(c => c.CorrelationId == _rule.Id)
            //    .Match<DTO>(() => _dto)
            //    .Query(() => _fields, x => x
            //        .Match<FieldFact>()
            //        .Collect()
            //        .Where(c => c.Any()));

            Then()
                .Do(ctx => Process(_rule, _fields, ctx, _container));
        }
        private bool Test(IEnumerable<RuleFact> rules, IEnumerable<RuleObservedFact> observables, RuleType ruleType, out RuleFact fact)
        {
            var nextRule = rules.Where(p => !observables.Any(o => o.CorrelationId == p.Id));
            if (!nextRule.Any())
            {
                fact = null;
                return false;
            }
            var nextRuleStage = nextRule?.Min(m => m.ProcessingStage) ?? 0;
            var currentRule = rules.FirstOrDefault(p => p.ProcessingStage == nextRuleStage);
            if (currentRule.RuleType == ruleType)
            {
                fact = currentRule;
                return true;
            }
            //check rule type, return ifMatch
            fact = null;
            return false;
        }
        protected abstract void Process(RuleFact rule, IEnumerable<FieldFact> fields, IContext ctx,
            IServiceProvider container);
    }
}
