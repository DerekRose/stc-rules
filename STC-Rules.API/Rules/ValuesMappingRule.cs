﻿using NRules.Fluent.Dsl;
using NRules.RuleModel;
using STC_Rules.API.Facts;
using STC_Rules.API.Models;
using STC_Rules.API.Providers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace STC_Rules.API.Rules
{
    public class ValuesMappingRule : RuleBase
    {
        public ValuesMappingRule() : base(RuleType.VALUES_MAPPING)
        {

        }

        protected override void Process(RuleFact rule, IEnumerable<FieldFact> fields, IContext ctx,
            IServiceProvider container)
        {
            ResultFact result = null;
            try
            {
                var grpc = (IGrpcProvider)container.GetService(typeof(IGrpcProvider));
                var mappedValues = grpc
                    .MapValues(fields.Where(p => rule.Identifiers.Contains(p.Identifier)).Select(p => p.Value ?? string.Empty));
                foreach (var mapping in mappedValues)
                {
                    var field = fields.SingleOrDefault(p =>
                        string.Equals(p.Value, mapping.Key, StringComparison.InvariantCultureIgnoreCase));
                    field.Value = mapping.Value;
                }
                result = new ResultFact(true, RuleType.VALUES_MAPPING);
            }
            catch (Exception ex)
            {
                result = new ResultFact(false, RuleType.VALUES_MAPPING, ex.Message);
            }
            ctx.Insert(new RuleObservedFact(rule.Id, result));
        }
    }
}
