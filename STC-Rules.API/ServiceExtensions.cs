﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Text.Json;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using Newtonsoft.Json.Linq;
using STC_Rules.API.Storage;
using STC_Rules.API.Facts;
using STC_Rules.API.Models;
using NRules;
using NRules.Fluent;
using STC_Rules.API.Rules;
using STC_Rules.API.Providers;

namespace STC_Rules.API
{
    public static class ServiceExtentions
    {
        public static IDictionary<string, string> ToDictionary(this Dictionary<string, JsonElement> model)
        {
            if (model == null)
                throw new NotImplementedException();

            var dictionary = new Dictionary<string, string>();
            AddPropertiesToDictionary(new List<string> { }, model, dictionary);


            return dictionary;
        }

        private static void AddPropertiesToDictionary(IList<string> path, Dictionary<string, JsonElement> source, IDictionary<string, string> dictionary)
        {
            foreach (var obj in source)
            {
                var txt = obj.Value.GetRawText();
                if (obj.Value.ValueKind == JsonValueKind.Object)
                {
                    path.Add(obj.Key);
                    var props = obj.Value.EnumerateObject().ToDictionary(k => k.Name, v => (JsonElement)v.Value);
                    AddPropertiesToDictionary(path, props, dictionary);
                }
                if (obj.Value.ValueKind == JsonValueKind.String)
                {

                    dictionary.Add((path.Any() ? string.Join(".", path) + "." : "") + obj.Key, txt);
                }
            }
        }
        public static IServiceCollection AddRemoteGRPCConfiguration(this IServiceCollection services)
        {
            var grpc = new GrpcProvider();
            services.AddSingleton(grpc);
            return services;
        }

        public static IServiceCollection AddRuleFactoryConfiguration(this IServiceCollection services)
        {
            var repository = new RuleRepository();
            repository.Load(x => x.From(typeof(RequiredRule).Assembly));
            services.AddSingleton(repository.Compile());
            services.AddScoped(s => s.GetService<ISessionFactory>().CreateSession());
            
            return services;
        }
        public static IServiceCollection AddMongoConfiguration(this IServiceCollection services)
        {
            services.Configure<MongoDBConnectionOptions>(options =>
            {
                //TODO: add env var
                options.DatabaseName = "STC";
                options.RuleFactCollectionName = "rules";
                options.RulesetCollectionName = "rulesets";
                options.FieldFactCollectionName = "fields";
            });

            var uri = "mongodb://localhost:27017";

            // TODO: These only ever have one instance over the lifetime of the app.
            services.AddSingleton<IMongoClient>(_ => new MongoClient(uri));
            services.AddSingleton<IRepository<RuleFact>, RuleFactRepository>();
            services.AddSingleton<IRepository<Ruleset>, RulesetRepository>();
            services.AddSingleton<IRepository<FieldFact>, FieldFactRepository>();

            return services;
        }

        public static IDictionary<string, string> ToDictionary<T>(this T source)
        {
            var dictionary = new Dictionary<string, string>();
            if (source == null) return dictionary;
            AddPropertiesToDictionary<T>(new List<string>(), source, dictionary);
            return dictionary;
        }

        private static void AddPropertiesToDictionary<T>(IList<string> path, T source, IDictionary<string, string> dictionary)
        {
            var props = source.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);
            foreach (var property in props)
            {
                var value = property.GetValue(source, null);
                if (typeof(string) == property.PropertyType)
                {
                    dictionary.Add((path.Any() ? string.Join(".", path) + "." : "") + property.Name, (string)value);
                }
                else
                {
                    path.Add(property.Name);
                    AddPropertiesToDictionary(path, value, dictionary);
                }
            }
        }
    }
}
