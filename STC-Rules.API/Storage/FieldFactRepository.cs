﻿using Microsoft.Extensions.Options;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Driver;
using STC_Rules.API.Facts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STC_Rules.API.Storage
{
    public class FieldFactRepository : IRepository<FieldFact>
    {
        private readonly MongoDBConnectionOptions _options;
        private IMongoClient _client;
        private IMongoCollection<FieldFact> _collection;

        public FieldFactRepository(IMongoClient client, IOptions<MongoDBConnectionOptions> options)
        {
            BsonClassMap.RegisterClassMap<FieldFact>(cm =>
            {
                cm.AutoMap();
                var member = cm.GetMemberMap(x => x.Id);
                member.SetIdGenerator(GuidGenerator.Instance);
                cm.SetIdMember(member);
                cm.SetIgnoreExtraElements(true);
            });
            _options = options.Value;
            _client = client;
            _collection = _client.GetDatabase(_options.DatabaseName).GetCollection<FieldFact>(_options.FieldFactCollectionName);
        }

        public IQueryable<FieldFact> AsQueryable()
        {
            return _collection.AsQueryable();
        }

        public FieldFact Save(FieldFact model)
        {
            model.Values.RemoveAll(p => true);
            model.Results.RemoveAll(p => true);
            _collection.InsertOne(model);

            return model;
        }

        public FieldFact Update(FieldFact model)
        {
            var resource = _collection
                 .FindOneAndUpdate<FieldFact>(p => p.Id == model.Id,
                     Builders<FieldFact>.Update
                     .Set(e => e.Identifier, model.Identifier)
                     .Set(e => e.FieldType, model.FieldType)
                     .Set(e => e.Index, model.Index),
                     new FindOneAndUpdateOptions<FieldFact> { ReturnDocument = ReturnDocument.After });

            return resource;
        }
    }
}
