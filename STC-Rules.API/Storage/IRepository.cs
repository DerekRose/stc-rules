﻿using System.Linq;

namespace STC_Rules.API.Storage
{
    public interface IRepository<T>
    {
        public IQueryable<T> AsQueryable();
        public T Save(T model);
        public T Update(T model);
    }
}
