﻿namespace STC_Rules.API.Storage
{
    public class MongoDBConnectionOptions
    {
        public string DatabaseName { get; set; }
        public string RuleFactCollectionName { get; set; }
        public string RulesetCollectionName { get; set; }
        public string FieldFactCollectionName { get; set; }
    }
}
