﻿using Microsoft.Extensions.Options;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Driver;
using STC_Rules.API.Facts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STC_Rules.API.Storage
{
    public class RuleFactRepository : IRepository<RuleFact>
    {
        private readonly MongoDBConnectionOptions _options;
        private IMongoClient _client;
        private IMongoCollection<RuleFact> _collection;

        public RuleFactRepository(IMongoClient client, IOptions<MongoDBConnectionOptions> options)
        {
            BsonClassMap.RegisterClassMap<RuleFact>(cm =>
            {
                cm.AutoMap();
                var member = cm.GetMemberMap(x => x.Id);
                member.SetIdGenerator(GuidGenerator.Instance);
                cm.SetIdMember(member);
                cm.SetIgnoreExtraElements(true);
            });
            _options = options.Value;
            _client = client;
            _collection = _client.GetDatabase(_options.DatabaseName).GetCollection<RuleFact>(_options.RuleFactCollectionName);
        }
        public IQueryable<RuleFact> AsQueryable()
        {
            return _collection.AsQueryable();
        }

        public RuleFact Save(RuleFact model)
        {
            _collection.InsertOne(model);

            return model;
        }

        public RuleFact Update(RuleFact model)
        {
            var resource = _collection
                 .FindOneAndUpdate<RuleFact>(p => p.Id == model.Id,
                     Builders<RuleFact>.Update
                     .Set(e => e.Attributes, model.Attributes)
                     .Set(e => e.Identifiers, model.Identifiers),
                     new FindOneAndUpdateOptions<RuleFact> { ReturnDocument = ReturnDocument.After });

            return resource;
        }
    }
}
