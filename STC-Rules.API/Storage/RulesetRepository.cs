﻿using Microsoft.Extensions.Options;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Driver;
using STC_Rules.API.Models;
using System.Linq;

namespace STC_Rules.API.Storage
{
    public class RulesetRepository : IRepository<Ruleset>
    {
        private readonly MongoDBConnectionOptions _options;
        private IMongoClient _client;
        private IMongoCollection<Ruleset> _collection;

        public RulesetRepository(IMongoClient client, IOptions<MongoDBConnectionOptions> options)
        {
            BsonClassMap.RegisterClassMap<Ruleset>(cm =>
            {
                cm.AutoMap();
                var member = cm.GetMemberMap(x => x.Id);
                member.SetIdGenerator(GuidGenerator.Instance);
                cm.SetIdMember(member);
                cm.SetIgnoreExtraElements(true);
            });
            _options = options.Value;
            _client = client;
            _collection = _client.GetDatabase(_options.DatabaseName).GetCollection<Ruleset>(_options.RulesetCollectionName);
        }

        public IQueryable<Ruleset> AsQueryable()
        {
            return _collection.AsQueryable();
        }

        public Ruleset Save(Ruleset model)
        {
            _collection.InsertOne(model);

            return model;
        }

        public Ruleset Update(Ruleset model)
        {
            var resource = _collection
                 .FindOneAndUpdate<Ruleset>(p => p.Id == model.Id,
                     Builders<Ruleset>.Update
                     .Set(e => e.Name, model.Name)
                     .Set(e => e.RuleConfiguration, model.RuleConfiguration)
                     .Set(e => e.Attributes, model.Attributes),
                     new FindOneAndUpdateOptions<Ruleset> { ReturnDocument = ReturnDocument.After });

            return resource;
        }
    }
}
