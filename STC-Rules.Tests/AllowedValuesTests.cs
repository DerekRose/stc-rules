﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NRules;
using NRules.Fluent;
using Simple.Mocking;
using STC_Rules.API;
using STC_Rules.API.Facts;
using STC_Rules.API.Models;
using STC_Rules.API.Providers;
using STC_Rules.API.Rules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace STC_Rules.Tests
{
    [TestClass]
    public class AllowedValuesTests
    {
        private RuleFact _allowedValuesRule;
        private Ruleset _ruleset;
        //private DTO _dto;
        private IEnumerable<FieldFact> _fields;
        private RuleConfigurationFact _config;
        private ISessionFactory _factory;
        private IGrpcProvider _grpc;

        [TestInitialize]
        public void Initialize()
        {
            _ruleset = new Ruleset
            {
                RuleConfiguration = new RuleConfigurationFact
                {
                    RunBasicRules = true
                }
            };
            _allowedValuesRule = new RuleFact()
            {
                RuleType = RuleType.ALLOWED_VALUES,
                Identifiers = new List<string> { "TestValue1", "TestValue4" },
                Id = Guid.NewGuid()
            };

            //_dto = new DTO
            //{
            //    TestValue1 = "this passes",
            //    TestValue3 = "ThIs DoEs NoT pAsS",
            //    TestValue4 = "this also passes"
            //};

            _fields = new Dictionary<string, string> { { "TestValue1", "this passes" },{ "TestValue3", "ThIs DoEs NoT pAsS" },{ "TestValue4", "this also passes" } }
                .Select(p => new FieldFact
                {
                    Identifier = p.Key,
                    Value = p.Value
                });
            _config = new RuleConfigurationFact { RunBasicRules = true };
            _grpc = Mock.Interface<IGrpcProvider>();
            var container = Mock.Interface<IServiceProvider>();
            Expect.MethodCall(() => container.GetService(typeof(IGrpcProvider))).Returns(_grpc);
            Expect.MethodCall(() => container.GetService(typeof(IServiceProvider))).Returns(container);
            
            var repository = new RuleRepository();
            repository.Load(x => x.From(typeof(RequiredRule).Assembly));
            _factory = repository.Compile();
            _factory.DependencyResolver = new DependencyResolver(container);
        }

        [TestMethod]
        public void PassesAllowedValues()
        {
            _allowedValuesRule.Attributes = new Dictionary<string, string> {
                    { "listId", Guid.NewGuid().ToString() },
                    { "caseSensative", "true"}
                };
            Expect.MethodCall(() => _grpc.GetList(Any<Guid>.Value)).Returns(new List<string> { "this passes", "this also passes", "ThIs DoEs NoT pAsS" });


            var session = _factory.CreateSession();
            session.Insert(_ruleset);
            //Insert facts into rules engine's memory
            session.Insert(_allowedValuesRule);
            //session.Insert(grpc);
            session.Insert(_config);
            //session.Insert(container);
            //session.Insert(_dto);

            foreach (var field in _fields)
            {
                session.Insert(field);
            }

            var result = session.Fire();
            //should have 4 field facts now, 
            //TestValue1 is valid, 
            //TestValue2 is valid since it is not listed to be processed in the rule definition
            //TestValue3 is valid since it is not listed to be processed in the rule definition
            //TestValue4 is valid
            var facts = session.Query<FieldFact>().ToList();
            Assert.IsTrue(facts.All(p => p.Result.Valid));
        }
    }
}
