using Microsoft.VisualStudio.TestTools.UnitTesting;
using NRules;
using NRules.Fluent;
using STC_Rules.API;
using STC_Rules.API.Rules;
using STC_Rules.API.Facts;
using STC_Rules.API.Models;
using STC_Rules.API.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using Simple.Mocking;

namespace STC_Rules.Tests
{
    [TestClass]
    public class RequiredFieldsTests
    {
        private RuleFact _requiredRule;
        private Ruleset _ruleset;
        private DTO _dto;
        private IEnumerable<FieldFact> _fields;
        private RuleConfigurationFact _config;
        private ISessionFactory _factory;

        [TestInitialize]
        public void Initialize()
        {
            _ruleset = new Ruleset
            {
                RuleConfiguration = new RuleConfigurationFact
                {
                    RunBasicRules = true
                }
            };
            _requiredRule = new RuleFact()
            {
                RuleType = RuleType.REQUIRED,
                Identifiers = new List<string> { "TestValue1", "TestValue4" },
                Id = Guid.NewGuid()
            };

            //_dto = new DTO
            //{
            //    TestValue1 = "this passes",
            //    TestValue3 = "",
            //    TestValue4 = "this also passes"
            //};

            _fields = new Dictionary<string, string> { { "TestValue1", "this passes" }, { "TestValue2", "this passes" }, { "TestValue3", "" }, { "TestValue4", "this also passes" } }
                .Select(p => new FieldFact
                {
                    Identifier = p.Key,
                    Value = p.Value
                });
            _config = new RuleConfigurationFact { RunBasicRules = true };

           
            var container = Mock.Interface<IServiceProvider>();
            Expect.MethodCall(() => container.GetService(typeof(IServiceProvider))).Returns(container);
            var repository = new RuleRepository();
            repository.Load(x => x.From(typeof(RequiredRule).Assembly));
            _factory = repository.Compile();
            _factory.DependencyResolver = new DependencyResolver(container);
        }

        [TestMethod]
        public void PassesRequiredFields()
        {
            var session = _factory.CreateSession();

            //Insert facts into rules engine's memory
            session.Insert(_ruleset);
            session.Insert(_requiredRule);
            //session.Insert(grpc);
            //session.Insert(_config);
            //session.Insert(container);
            //session.Insert(_dto);

            foreach (var field in _fields)
            {
                session.Insert(field);
            }

            var result = session.Fire();
            //should have 4 field facts now, 
            //TestValue1 is valid, 
            //TestValue2 is valid since it is not required, 
            //TestValue3 is invalid since the value wasnt set
            //TestValue4 is invalid (missing field defaults to invalid)
            var facts = session.Query<FieldFact>().ToList();
            Assert.IsTrue(facts.All(p => p.Result.Valid));
        }

        [TestMethod]
        public void MissingRequiredFields()
        {
            var session = _factory.CreateSession();
            _requiredRule.Identifiers.Add("TestValue3");
            var invalidField = "TestValue4";
            //Insert facts into rules engine's memory
            session.Insert(_ruleset);
            session.Insert(_requiredRule);
            //session.Insert(grpc);
            session.Insert(_config);
            //session.Insert(container);
            //session.Insert(_dto);
            foreach (var field in _fields)
            {
                //leave a required field out
                if (field.Identifier != invalidField)
                {
                    session.Insert(field);
                }
            }

            var result = session.Fire();
            //should have 4 field facts now, 
            //TestValue1 is valid, 
            //TestValue2 is valid since it is not required, 
            //TestValue3 is invalid since the value wasnt set
            //TestValue4 is invalid (missing field defaults to invalid)
            var facts = session.Query<FieldFact>().ToList();
            Assert.IsTrue(facts.Count(p => p.Result.Valid) == 2);
            Assert.IsTrue(facts.Where(p => !p.Result.Valid).All(p => p.Identifier == "TestValue4" || p.Identifier == "TestValue3"));
        }
    }
}
