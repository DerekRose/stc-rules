﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using NRules;
using NRules.Fluent;
using Simple.Mocking;
using STC_Rules.API;
using STC_Rules.API.Controllers;
using STC_Rules.API.Facts;
using STC_Rules.API.Models;
using STC_Rules.API.Providers;
using STC_Rules.API.Rules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace STC_Rules.Tests
{
    [TestClass]
    public class RuleIntegrationTests
    {
        //private DTO _dto;
        private List<FieldFact> _fields;
        private Ruleset _ruleset;
        private ISessionFactory _factory;
        private IGrpcProvider _grpc;
        private List<RuleFact> _rules;

        [TestInitialize]
        public void Initialize()
        {
            _ruleset = new Ruleset
            {
                RuleConfiguration = new RuleConfigurationFact
                {
                    RunBasicRules = true
                }
            };
            _rules = new List<RuleFact>
            {
                new RuleFact()
                {
                    RuleType = RuleType.ALLOWED_VALUES,
                    Identifiers = new List<string> { "TestValue1", "TestValue4" },
                    Id = Guid.NewGuid(),
                    ProcessingStage = 2,
                    Attributes = new Dictionary<string, string>
                    {
                        { "listId", Guid.NewGuid().ToString() },
                        { "caseSensative", "true"}
                    }
                },
                new RuleFact()
                {
                    RuleType = RuleType.REQUIRED,
                    Identifiers = new List<string> { "Test.Test1.Test2" },
                    Id = Guid.NewGuid(),
                    ProcessingStage = 1
                },
                new RuleFact()
                {
                    RuleType = RuleType.POCO_MAPPING,
                    Identifiers = new List<string> { "Test.Test1.Test2:TestValue2" },
                    Id = Guid.NewGuid(),
                    ProcessingStage = 0,
                    Attributes = new Dictionary<string, string>
                    {
                        { "pocoMappings", ""}
                    }
                },
                new RuleFact()
                {
                    RuleType = RuleType.REQUIRED,
                    Identifiers = new List<string> { "Test.Test1.Test2" },
                    Id = Guid.NewGuid(),
                    ProcessingStage = 3
                }

            };

            //_dto = new DTO
            //{
            //    TestValue1 = "this passes",
            //    TestValue3 = "ThIs DoEs NoT pAsS",
            //    TestValue4 = "this also passes"
            //};

            _grpc = Mock.Interface<IGrpcProvider>();
            var container = Mock.Interface<IServiceProvider>();
            Expect.MethodCall(() => container.GetService(typeof(IServiceProvider))).Returns(container);
            var repository = new RuleRepository();
            repository.Load(x => x.From(typeof(RequiredRule).Assembly));
            _factory = repository.Compile();
            _factory.DependencyResolver = new DependencyResolver(container);
            Expect.MethodCall(() => _grpc.GetList(Any<Guid>.Value)).Returns(new List<string> { "this passes", "this also passes", "ThIs DoEs NoT pAsS" });
        }

        [TestMethod]
        public void PassesAllRules()
        {
            var json = JsonSerializer.Serialize(new Dictionary<string, object> { { "Test", new { Test1 = new { Test2 = "testing" } } } });
            var dto = JsonSerializer.Deserialize<Dictionary<string, object>>(json);
            var test = new { DTO = dto };

            var dictionary = test.DTO.ToDictionary(k => k.Key, p => (JsonElement)p.Value).ToDictionary();

            var session = _factory.CreateSession();
            _fields = dictionary.Select(p => new FieldFact
            {
                Identifier = p.Key,
                Value = p.Value
            }).ToList();

            _rules.ForEach(p => session.Insert(p));
            session.Insert(_ruleset);
            //session.Insert(_dto);
            _fields.ForEach(p => session.Insert(p));

            var result = session.Fire();
            var facts = session.Query<FieldFact>().ToList();
            var observedFacts = session.Query<RuleObservedFact>().ToList();
            Assert.IsTrue(_rules.All(i => observedFacts.Select(p => p.CorrelationId).Contains(i.Id)) && _rules.Count == observedFacts.Count);
            Assert.IsTrue(facts.All(p => p.Result.Valid));
        }
    }
}
